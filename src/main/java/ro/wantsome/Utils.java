package ro.wantsome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromResourcesFile(String fileName) throws IOException {
        List<String> result = new ArrayList<>();
        ClassLoader classLoader = Main.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        }
        return result;
    }

    /**
     * Can read a given file in the working directory
     *
     * @param fileName file name
     * @return list of all the lines in the file
     */
    public static List<String> readAllLinesFromSourceFile(String fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordSet = new HashSet<>();
        for (String line : allLines) {
            wordSet.add(line);

        }
        return wordSet;
    }

    public static Set<String> findWordsThatContains(Set<String> wordSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordSet) {
            if (line.contains(word))
                result.add(line);
        }
        return result;
    }

    //verify if two words are palindromes
    public static Set<String> findPalindromes(Set<String> wordsList) {
        Set<String> result = new HashSet<>();
        if (wordsList == null) {
            System.out.println("Warning: null parameter for findPalindromes()");
            return result;
        }
        for (String line : wordsList) {
            StringBuilder reversedWord = new StringBuilder(line).reverse();
            if (line.equals(reversedWord.toString()))
                result.add(line);
        }
        return result;

    }

    private static String sortLetters(String word) {
        return word.chars().sorted()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

    }

    public static Map<String, List<String>> findAnagrams(Set<String> wordSet, String sample) {
        Map<String, List<String>> anagrams = new HashMap<>();

        String sortedSample = null;
        if (sample != null && sample.trim() != "") sortedSample = sortLetters(sample.trim());

        if (wordSet == null) {
            System.out.println("Warning: null parameter for findAnagrams()");
            return anagrams;
        }
        for (String word : wordSet
        ) {
            String sortedWord = sortLetters(word);
            //if(sortedSample != null && !sortedSample.equals(sortedWord)) continue;

            List<String> currentAnagramsForKey = anagrams.getOrDefault(sortedWord, new ArrayList<>());
            currentAnagramsForKey.add(word);
            if (!anagrams.containsKey(sortedWord)) {
                anagrams.put(sortedWord, currentAnagramsForKey);
            }

        }

        return anagrams;
    }

    public static List<String> sortSet(Set<String> unsorted) {
        List<String> result = new ArrayList<>(unsorted);
        Collections.sort(result);
        return result;
    }

    public static String getRandomWordFromSet(Set<String> wordSet) {
        Random random = new Random();
        return getRandomWordFromSet(wordSet, random);

    }

    public static String getRandomWordFromSet(Set<String> wordSet, Random random) {
        List<String> wordList = new ArrayList<>(wordSet);
        int index = random.nextInt(wordList.size());
        return wordList.get(index);

    }

    public static String getRandomWord(Set<String> wordSet) {
        return null;
    }
}
