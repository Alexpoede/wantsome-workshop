package ro.wantsome.fileReader;

        import java.io.IOException;
        import java.nio.file.Files;
        import java.nio.file.Paths;
        import java.util.List;

public class GlobalInputFileReader implements InputFileReader {
    @Override
    public List<String> readFile(String input) throws IOException {
        return Files.readAllLines(Paths.get(input));
    }
}
