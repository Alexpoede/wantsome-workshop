package ro.wantsome;

import ro.wantsome.dictionaryOperation.DictionaryOperation;
import ro.wantsome.dictionaryOperation.PalindromDictionaryOperation;
import ro.wantsome.dictionaryOperation.RandomWordDictionaryOperation;
import ro.wantsome.dictionaryOperation.SearchDictionaryOperation;
import ro.wantsome.fileReader.GlobalInputFileReader;
import ro.wantsome.fileReader.InputFileReader;
import ro.wantsome.fileReader.ResourceInputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] arg) throws IOException {
        List<String> allLines = new ArrayList<>();
        InputFileReader inputFileReader = new ResourceInputFileReader();
        allLines = inputFileReader.readFile("dex.txt");
        //la fel ca:
        //InputFileReader inputFileReader = new GlobalInputFileReader();
        // allLines = inputFileReader.readFile("C:\\Code\\WantsomeWorkshop\\src\\main\\resources\\dex.txt");


        allLines = Utils.readAllLinesFromResourcesFile("dex.txt");

        Set<String> wordSet = Utils.removeDuplicates(allLines);

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Palindromes\n\t" +
                    "3. Anagrams\n\t" +
                    "4. Random\n\t"+
                    "0. Exit");

            String userMenuSelection = in.readLine();

            if (userMenuSelection.equals("0")) {
                System.out.println("Exiting...");
                break;
            }

            DictionaryOperation operation = null;
            switch (userMenuSelection) {
                case "1":
                    operation = new SearchDictionaryOperation(wordSet, in);
                    break; //or exit(0);
                case "2":
                    operation = new PalindromDictionaryOperation(wordSet);
                    break;

                case "3":
                    System.out.println("Anagrams for: ");
                    String anagramsForWord = in.readLine();
                    System.out.println("Anagrams");
                    Map<String, List<String>> anagrams =
                            Utils.findAnagrams(wordSet, anagramsForWord);

                    for (String key : anagrams.keySet()) {
                        System.out.println("Anagrams for " + key + " : ");
                        List<String> words = anagrams.get(key);
                        for (String word : words) {
                            System.out.print(word + " ");
                        }
                        System.out.println();
                    }
                    break;
                case "4":
                    operation = new RandomWordDictionaryOperation(wordSet);
                    break;

                default:
                    System.out.println("Please select a valid option");

            }
            if (operation != null) {
                operation.run();

            }

        }

    }
}

