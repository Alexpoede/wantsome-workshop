package ro.wantsome.dictionaryOperation;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class PalindromDictionaryOperation implements DictionaryOperation {
    private Set<String> wordSet;

    public PalindromDictionaryOperation(Set<String> wordSet) {

        this.wordSet = wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Palindromes");
        Set<String> palindromes =
                Utils.findPalindromes(wordSet);
        List<String> sortedPalindromes = Utils.sortSet(palindromes);
        for (String line : sortedPalindromes) {
            System.out.println(line);
        }
    }
}
