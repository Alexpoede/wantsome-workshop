package ro.wantsome.dictionaryOperation;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class RandomWordDictionaryOperation implements DictionaryOperation {
    private Set<String> wordSet;

    public RandomWordDictionaryOperation(Set<String> wordSet) {

        this.wordSet = wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Random word");
        String randomWord = Utils.getRandomWord(wordSet);
            System.out.println(randomWord);
        }
    }

