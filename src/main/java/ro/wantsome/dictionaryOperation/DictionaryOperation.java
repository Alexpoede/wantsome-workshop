package ro.wantsome.dictionaryOperation;

import java.io.IOException;

public interface DictionaryOperation {
    void run() throws IOException;
}
