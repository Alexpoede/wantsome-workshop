package ro.wantsome.dictionaryOperation;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class SearchDictionaryOperation implements DictionaryOperation {
    private BufferedReader in;
    private Set<String> wordSet;

    public SearchDictionaryOperation(Set<String> wordSet, BufferedReader in){
        this.in = in;
        this.wordSet= wordSet;
    }

    @Override
    public void run() throws IOException{
        System.out.println("Searching");
        String userGivenWord = in.readLine();
        if ("".equals(userGivenWord))
           return;

        Set<String> resultsWords =
                Utils.findWordsThatContains(wordSet, userGivenWord);
        List<String> sortedWords = Utils.sortSet(resultsWords);
        for (String line : sortedWords) {
            System.out.println(line);
        }

    }
}
